<%-- 
    Document   : lista
    Created on : 24-04-2021, 18:01:47
    Author     : patiw
--%>

<%@page import="java.util.Iterator"%>
<%@page import="root.entity.Consultamedica"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Consultamedica> consultas = (List<Consultamedica>) request.getAttribute("listaconsultas");
    Iterator<Consultamedica> itconsultas = consultas.iterator();
%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form  name="form" action="controlador" method="POST">
            <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">


                <table border="1">
                    <thead>
                    <th>Rut</th>
                    <th>Nombre completo </th>

                    <th>Apellidos</th>
                    <th>Telefono </th>

                    <th>Direccion</th>
                    <th>Prevision </th>


                    </thead>
                    </tbody>
                    <%while (itconsultas.hasNext()) {
                            Consultamedica cm = itconsultas.next();%>

                    <tr>
                        <td><%= cm.getRut()%></td>
                        <td><%= cm.getNombres()%></td>
                        <td><%= cm.getApellidos()%></td>
                        <td><%= cm.getTelefono()%></td>
                        <td><%= cm.getDireccion()%></td>    
                        <td><%= cm.getPrevision()%></td>

                        <td> <input type="radio" name="seleccion" value="<%= cm.getRut()%>"> </td>
                    </tr>
                    <%}%>                
                    </tbody>   
                </table>
                <button type="submit" name="accion"  style="background-color:palegreen"  value="eliminar" class="btn btn-success">Eliminar consulta</button>
                <button type="submit" name="accion" style="background-color:palegreen"  value="Consultar" class="btn btn-success">Consultar</button>  

        </form>            
    </body>
</html>