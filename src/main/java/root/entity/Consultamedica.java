/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author patiw
 */
@Entity
@Table(name = "consultamedica")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consultamedica.findAll", query = "SELECT c FROM Consultamedica c"),
    @NamedQuery(name = "Consultamedica.findByRut", query = "SELECT c FROM Consultamedica c WHERE c.rut = :rut"),
    @NamedQuery(name = "Consultamedica.findByNombres", query = "SELECT c FROM Consultamedica c WHERE c.nombres = :nombres"),
    @NamedQuery(name = "Consultamedica.findByApellidos", query = "SELECT c FROM Consultamedica c WHERE c.apellidos = :apellidos"),
    @NamedQuery(name = "Consultamedica.findByTelefono", query = "SELECT c FROM Consultamedica c WHERE c.telefono = :telefono"),
    @NamedQuery(name = "Consultamedica.findByDireccion", query = "SELECT c FROM Consultamedica c WHERE c.direccion = :direccion"),
    @NamedQuery(name = "Consultamedica.findByPrevision", query = "SELECT c FROM Consultamedica c WHERE c.prevision = :prevision")})
public class Consultamedica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "nombres")
    private String nombres;
    @Size(max = 2147483647)
    @Column(name = "apellidos")
    private String apellidos;
    @Size(max = 2147483647)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 2147483647)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 2147483647)
    @Column(name = "prevision")
    private String prevision;

    public Consultamedica() {
    }

    public Consultamedica(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPrevision() {
        return prevision;
    }

    public void setPrevision(String prevision) {
        this.prevision = prevision;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consultamedica)) {
            return false;
        }
        Consultamedica other = (Consultamedica) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.entity.Consultamedica[ rut=" + rut + " ]";
    }
    
}
