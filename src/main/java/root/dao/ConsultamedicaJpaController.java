/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.dao.exceptions.NonexistentEntityException;
import root.dao.exceptions.PreexistingEntityException;
import root.entity.Consultamedica;

/**
 *
 * @author patiw
 */
public class ConsultamedicaJpaController implements Serializable {
    
    
 private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");


    public ConsultamedicaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }

    
    public ConsultamedicaJpaController(){
     }
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Consultamedica consultamedica) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(consultamedica);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findConsultamedica(consultamedica.getRut()) != null) {
                throw new PreexistingEntityException("Consultamedica " + consultamedica + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Consultamedica consultamedica) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            consultamedica = em.merge(consultamedica);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = consultamedica.getRut();
                if (findConsultamedica(id) == null) {
                    throw new NonexistentEntityException("The consultamedica with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Consultamedica consultamedica;
            try {
                consultamedica = em.getReference(Consultamedica.class, id);
                consultamedica.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The consultamedica with id " + id + " no longer exists.", enfe);
            }
            em.remove(consultamedica);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Consultamedica> findConsultamedicaEntities() {
        return findConsultamedicaEntities(true, -1, -1);
    }

    public List<Consultamedica> findConsultamedicaEntities(int maxResults, int firstResult) {
        return findConsultamedicaEntities(false, maxResults, firstResult);
    }

    private List<Consultamedica> findConsultamedicaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Consultamedica.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Consultamedica findConsultamedica(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Consultamedica.class, id);
        } finally {
            em.close();
        }
    }

    public int getConsultamedicaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Consultamedica> rt = cq.from(Consultamedica.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public Consultamedica edit(String rut) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
